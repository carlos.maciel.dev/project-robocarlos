from iqoptionapi.stable_api import IQ_Option
import logging
import time
import json
import logging
import configparser
from datetime import datetime, date, timedelta
from dateutil import tz
import sys
import telegram
from credentials2 import telegram_token, chat_id

# LOGIN
# logging.basicConfig(level=logging.DEBUG,format='%(asctime)s %(message)s')
# I_want_money = IQ_Option("carlosbeto1996@gmail.com", "bebetinha1996")
I_want_money = IQ_Option("carlosbeto1996@gmail.com", "bebetinha1996")
I_want_money.connect()  # connect to iqoption
print(I_want_money.get_balance_id())
P = "PRACTICE"
R = "REAL"

# print(I_want_money.reset_practice_balance())

# CONFIG

# config_paridade = 'USDJPY-OTC'
# config_paridade = 'AUDJPY-OTC'
# config_paridade = 'GBPUSD-OTC'
# config_paridade = 'GBPJPY-OTC'
# config_paridade = 'EURJPY-OTC'
# config_paridade = 'EURGBP-OTC'
I_want_money.change_balance(R)
config_paridade = 'EURUSD'
config_valor_entrada = 2
entrada_inicial = 2
config_timeframe = 1

config_martingale = 'N'
config_sorosgale = 'N'
config_niveis = 2

config_stop_loss = 7
config_stop_win = 5

config_valor_minimo = 20
config_filtro_pais = 'todos'
config_filtro_top_traders = 20000
config_filtro_diferenca_sinal = 2
config_type = 'PT1M'  # "PT1M"/"PT5M"/"PT15M"
config_seguir_ids = ''

# ROBO
horaAtual = datetime.now()

bot = telegram.Bot(token=telegram_token)
# mande uma mensagem
bot.sendMessage(chat_id=chat_id, text='\U00002747' + ' ' + str(horaAtual.day) + '/' + str(horaAtual.month) + '/' + str(horaAtual.year) + '|' + str(horaAtual.hour) + ':' + str(horaAtual.minute) + ':' + str(horaAtual.second)  + ' ' + '\U00002747')
bot.sendMessage(chat_id=chat_id, text='\U00002747 ATENÇÃO NOVA SEQUÊNCIA, INICIANDO EM 1 MINUTO \U00002747')
bot.sendMessage(chat_id=chat_id, text='\U00002757 Tempo: M1')
bot.sendMessage(chat_id=chat_id, text='\U00002757 PAR/MOEDA: ' + config_paridade + ' ' + '\U00002757')
bot.sendMessage(chat_id=chat_id, text='AGUARDE...')
time.sleep(30)

def timestamp_converter(x, retorno=1):
    hora = datetime.strptime(datetime.utcfromtimestamp(
        x).strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    hora = hora.replace(tzinfo=tz.gettz('GMT'))
    return str(hora.astimezone(tz.gettz('America/Sao Paulo')))[:-6] if retorno == 1 else hora.astimezone(tz.gettz('America/Sao Paulo'))

def valorEntradas(resultado):
    global config_valor_entrada
    
    if(resultado == 'win'):
        config_valor_entrada = entrada_inicial
    else:
        config_valor_entrada = config_valor_entrada * 2
    
        
    return config_valor_entrada 

def banca():
    valueBanca = I_want_money.get_balance();
    print(valueBanca)
    return I_want_money.get_balance()


def payout(par, tipo, timeframe=1):
    if tipo == 'turbo':
        a = I_want_money.get_all_profit()
        return int(100 * a[par]['turbo'])

    elif tipo == 'digital':

        I_want_money.subscribe_strike_list(par, timeframe)
        while True:
            d = I_want_money.get_digital_current_profit(par, timeframe)
            if d != False:
                d = int(d)
                break
            time.sleep(1)
        I_want_money.unsubscribe_strike_list(par, timeframe)
        return d


def configuracao():

    return {'seguir_ids': config_seguir_ids, 'stop_win': config_stop_win, 'stop_loss': config_stop_loss, 'payout': 0, 'banca_inicial': banca(), 'filtro_diferenca_sinal': config_filtro_diferenca_sinal, 'martingale': config_martingale, 'sorosgale': config_sorosgale, 'niveis': config_niveis, 'filtro_pais': config_filtro_pais, 'filtro_top_traders': config_filtro_top_traders, 'valor_minimo': config_valor_minimo, 'paridade': config_paridade, 'valor_entrada': config_valor_entrada, 'timeframe': config_timeframe}


def martingale(tipo, valor, payout):
    if tipo == 'simples':
        return valor * 2.2
    else:

        lucro_esperado = float(valor) * float(payout)
        perca = valor
        while True:
            if round(float(valor) * float(payout), 2) > round(float(abs(perca)) + float(lucro_esperado), 2):
                return round(valor, 2)
                break
            valor += 0.01


def entradas(config, entrada, direcao, timeframe):
    if direcao == 'call':
        dire = 'Vender'
        emot = '\U00002935'
        operacao = 'put' 
       
    else:
        dire = 'Comprar'        
        emot = '\U00002934'
        operacao = 'call'   
    bot.sendMessage(chat_id=chat_id, text= '\U00002734 CLIQUE AGORA \U00002734'+ '\n' + emot + 'DIREÇÃO: ' + dire + emot)

    status, id = I_want_money.buy_digital_spot(
        config['paridade'], entrada, operacao, timeframe)
    

    if status:
        # STOP WIN/STOP LOSS
        banca_att = banca()
        stop_loss = False
        stop_win = False

        if round((banca_att - float(config['banca_inicial'])), 2) <= (abs(float(config['stop_loss'])) * -1.0):
            print('stop loss = ', round(
                (banca_att - float(config['banca_inicial'])), 2))
            stop_loss = True

        if round((banca_att - float(config['banca_inicial'])) + (float(entrada) * float(config['payout'])) + float(entrada), 2) >= abs(float(config['stop_win'])):
            print('stop win = ', round((banca_att - float(config['banca_inicial'])) + (
                float(entrada) * float(config['payout'])) + float(entrada), 2))
            stop_win = True

        while True:
            status, lucro = I_want_money.check_win_digital_v2(id)

            if status:
                if lucro > 0:
                    # I_want_money.change_balance("REAL")
                    # print('Mudança para REAL')
                    
                    bot.sendMessage(chat_id=chat_id, text= '\U00002728 Parabéns você Lucrou \U00002764 ! Aguarde Próxima Sequência' + '\n'+ '\U00002714 \U00002714 \U00002714 \U00002714 \U00002714')
                    valorEntradas('win')
                    time.sleep(300)
                    horaAtual = datetime.now()

                    
                    # mande uma mensagem
                    bot.sendMessage(chat_id=chat_id, text='\U00002747' + ' ' + str(horaAtual.day) + '/' + str(horaAtual.month) + '/' + str(horaAtual.year) + '|' + str(horaAtual.hour) + ':' + str(horaAtual.minute) + ':' + str(horaAtual.second)  + ' ' + '\U00002747')
                    bot.sendMessage(chat_id=chat_id, text='\U00002747 ATENÇÃO NOVA SEQUÊNCIA, INICIANDO EM 1 MINUTO \U00002747')
                    bot.sendMessage(chat_id=chat_id, text='\U00002757 Tempo: M1')
                    bot.sendMessage(chat_id=chat_id, text='\U00002757 PAR/MOEDA: ' + config_paridade + ' ' + '\U00002757')
                    bot.sendMessage(chat_id=chat_id, text='AGUARDE...')
                    time.sleep(30)
                    return 'win', round(lucro, 2), stop_win
                    
                else:
                    # I_want_money.change_balance("REAL")
                    # print('Mudança para pratica')
                    valorEntradas('loss')
                    bot.sendMessage(chat_id=chat_id, text= 'PREJUÍZO !!! MULTIPLIQUE POR DOIS PARA PRÓXIMA OPERAÇÃO RÁPIDO!!' + '\n'+ '\U0000274c \U0000274c \U0000274c \U0000274c \U0000274c')
                                      
                    return 'loss', 0, stop_loss
                break

    else:
        return 'error', 0, False


# Carrega as configuracoes
config = configuracao()
config['banca_inicial'] = banca()


# Filtros
# 1? Filtro por valor da entrada copiada
# 2? Filtro para copiar entrada dos top X
# 3? Filtro Pais

# Captura os dados necessarios do ranking TRANDING MINIMOS
def filtro_ranking(config):

    user_id = []

    try:
        ranking = I_want_money.get_leader_board(
            'Worldwide' if config['filtro_pais'] == 'todos' else config['filtro_pais'].upper(), 1, int(config['filtro_top_traders']), 0)

        if int(config['filtro_top_traders']) != 0:
            for n in ranking['result']['positional']:
                id = ranking['result']['positional'][n]['user_id']
                user_id.append(id)
    except:
        pass

    return user_id


filtro_top_traders = filtro_ranking(config)

if config['seguir_ids'] != '':
    if ',' in config['seguir_ids']:
        x = config['seguir_ids'].split(',')
        for old in x:
            filtro_top_traders.append(int(old))
    else:
        filtro_top_traders.append(int(config['seguir_ids']))


tipo = 'live-deal-digital-option'  # live-deal-binary-option-placed
timeframe = config_type  # PT5M / PT15M
old = 0

# Captura o Payout
config['payout'] = float(
    payout(config['paridade'], 'digital', int(config['timeframe'])) / 100)

I_want_money.subscribe_live_deal(tipo, config['paridade'], timeframe, 10)

while True:
    trades = I_want_money.get_live_deal(tipo, config['paridade'], timeframe)

    if len(trades) > 0 and old != trades[0]['user_id'] and trades[0]['amount_enrolled'] >= float(config['valor_minimo']):
        ok = True

        # Correcao de bug em relacao ao retorno de datas errado
        res = round(time.time(
        ) - datetime.timestamp(timestamp_converter(trades[0]['created_at'] / 1000, 2)), 2)
        ok = True if res <= int(config['filtro_diferenca_sinal']) else False

        if len(filtro_top_traders) > 0:
            if trades[0]['user_id'] not in filtro_top_traders:
                ok = False

        if ok:
            # Dados sinal
            print(res, end='')
            print(' [', trades[0]['flag'], ']', config['paridade'], '/', trades[0]['amount_enrolled'],
                  '/', trades[0]['instrument_dir'], '/', trades[0]['name'], trades[0]['user_id'])

            # 1 entrada
            resultado, lucro, stop = entradas(
                config, config_valor_entrada, trades[0]['instrument_dir'], int(config['timeframe']))
            print('   -> ', resultado, '/', lucro, '\n\n')

            if stop:
                print('\n\nStop', resultado.upper(), 'batido!')
                sys.exit()

            # Martingale
            if resultado == 'loss' and config['martingale'] == 'S':
                valor_entrada = martingale('auto', float(
                    config['valor_entrada']), float(config['payout']))
                for i in range(int(config['niveis']) if int(config['niveis']) > 0 else 1):

                    print('   MARTINGALE NIVEL '+str(i+1)+'..', end='')
                    resultado, lucro, stop = entradas(
                        config, valor_entrada, trades[0]['instrument_dir'], int(config['timeframe']))
                    print(' ', resultado, '/', lucro, '\n')
                    if stop:
                        print('\n\nStop', resultado.upper(), 'batido!')
                        sys.exit()

                    if resultado == 'win':
                        print('\n')
                        break
                    else:
                        valor_entrada = martingale('auto', float(
                            valor_entrada), float(config['payout']))

            # SorosGale
            elif resultado == 'loss' and config['sorosgale'] == 'S':

                if float(config['valor_entrada']) > 5:

                    lucro_total = 0
                    lucro = 0
                    perca = float(config['valor_entrada'])
                    # Nivel
                    for i in range(int(config['niveis']) if int(config['niveis']) > 0 else 1):

                        # Mao
                        for i2 in range(2):

                            if lucro_total >= perca:
                                break

                            print('   SOROSGALE NIVEL '+str(i+1) +
                                  ' | MAO '+str(i2+1)+' | ', end='')

                            # Entrada
                            resultado, lucro, stop = entradas(
                                config, (perca / 2)+lucro, trades[0]['instrument_dir'], int(config['timeframe']))
                            print(resultado, '/', lucro, '\n')
                            if stop:
                                print('\n\nStop', resultado.upper(), 'batido!')
                                sys.exit()

                            if resultado == 'win':
                                lucro_total += lucro
                                

                else:
                    lucro_total = 0
                    perca += perca / 2
                    break

        old = trades[0]['user_id']
    time.sleep(0.2)

I_want_money.unscribe_live_deal(tipo, config['paridade'], timeframe)
