from iqoptionapi.stable_api import IQ_Option
import logging
import time
import json
import logging
import configparser
from datetime import datetime, date, timedelta
from dateutil import tz
import sys
import time, threading
from pytz import timezone

# import telegram
# importe suas chaves de acesso do arquivo credentials.py
# from credentials import telegram_token, chat_id
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db




#ROBO CARLOS MACIEL
cred = credentials.Certificate('firebase-sdk.json')

firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://automatize-44022.firebaseio.com'
})

# DONO ROBO
idUser = 'b2i1zO7Ax2WoeT8BKWZL3xDIdsA2'

#CONEXAO FIREBASE
ref = db.reference('usuario/')
snapshot = ref.get()

# SETANDO CONTA IQOPTION
emailUser = snapshot[idUser]['emailIQ'] 
passwordUser = snapshot[idUser]['passwordIQ']

#LOGIN CONTA
I_want_money = IQ_Option(emailUser, passwordUser)
I_want_money.connect()  # connect to iqoption

#STATUS ROBO
robo = snapshot[idUser]['robo']
status = robo['status']

#METODO DA OPERACAO
def action(hora, minutos, valorEntrada, parDeMoeda, duracao, operacao, tipoConta) : 
   horaAtual = datetime.now()
   
    # print(horaAtual.hour, ':', horaAtual.minute, ':', horaAtual.second)
   if (horaAtual.hour == int(hora)) and (horaAtual.minute == int(minutos)) and (horaAtual.second < 10) : 
        print('INICIOU PRIMEIRA OPERACAO')
        # bot.sendMessage(chat_id=chat_id, text='Sua operação de: '+ str(hora) + ':' + str(minutos) + ' Iniciou')
        # bot.sendMessage(chat_id=chat_id, text='ENTRADA: '+ str(valorEntrada))
        # bot.sendMessage(chat_id=chat_id, text='Par de Moeda: '+ parDeMoeda)
        # bot.sendMessage(chat_id=chat_id, text='Duração: '+ str(duracao))
        # bot.sendMessage(chat_id=chat_id, text='Direção: '+operacao)
        # bot.sendMessage(chat_id=chat_id, text='Conta: ' + tipoConta)
        I_want_money.change_balance(tipoConta)
        ACTIVES= parDeMoeda
        duration= int(duracao)
        amount= valorEntrada
        action= operacao
        segundaEntrada = valorEntrada * 2
        buy_check,id = I_want_money.buy_digital_spot(ACTIVES,amount,action,duration)
        if buy_check:
            print("wait for check win")
            #check win
            while True:
                check_close,win_money=I_want_money.check_win_digital_v2(id)
                if check_close:
                    if float(win_money)>0:
                        win_money=("%.2f" % (win_money))
                        print("Você ganhou",win_money,"Aguarde proxima Operação")
                        # mande uma mensagem
                        # bot.sendMessage(chat_id=chat_id, text='Você ganhou na operação: '+ str(hora) +  ':' + str(minutos) + ' O valor de:' + str(win_money))
                        # bot.sendMessage(chat_id=chat_id, text='Fim da operação, aguarde a próxima... ')

                        return 'FIM'
                    else:
                        # bot.sendMessage(chat_id=chat_id, text='Você perdeu na operação: '+ str(hora) + ':' + str(minutos) + ' O valor de:' + str(valorEntrada))
                        # bot.sendMessage(chat_id=chat_id, text='Iniciando Martingale de nivel 1 :' + '\n' + 'Valor de: ' + str(segundaEntrada))
                        print('MATINGALE')
                        martin_gale, idMartin = I_want_money.buy_digital_spot(ACTIVES,segundaEntrada,action,duration)
                        if martin_gale:
                            print("Verificando WIN MartinGale")
                            while True:
                                check_close_martin,win_money_martin=I_want_money.check_win_digital_v2(idMartin)
                                if check_close_martin:
                                    if float(win_money_martin)>0:
                                        win_money_martin =("%.2f" % (win_money_martin))
                                        print("Você ganhou",win_money_martin,"Aguarde proxima Operação")
                                        # bot.sendMessage(chat_id=chat_id, text='Você ganhou no martingale: '+ str(hora) +  ':' + str(minutos) + ' O valor de:' + str(win_money_martin))
                                        # bot.sendMessage(chat_id=chat_id, text='Fim da operação, aguarde a próxima... ')

                                        return 'FIM'
                                   
                                    else:
                                        # bot.sendMessage(chat_id=chat_id, text='Você perdeu no martingale ! '+ '\n' + 'Resultado: ' + str(win_money_martin) + ' Reais' )
                                        # bot.sendMessage(chat_id=chat_id, text='Fim da operação, aguarde a próxima... ')

                                        return 'FIM'
                                        break
        else:
            print("Falha ao operar") 
            return 'FIM'   




# ref = db.reference('usuario/')
# snapshot = ref.get()



        
   


while status == 'ATIVO':
        ref = db.reference('usuario/')
        op_ref = ref.child(idUser)
        opp_ref = op_ref.child('operacoes')
        snapshot = ref.get()
        robo = snapshot[idUser]['robo']
        status = robo['status']
        # print('ligado')
        operacoes = snapshot[idUser]['operacoes']
        for i in operacoes:
            oppp_ref = opp_ref.child(i)
            if operacoes[i]['status'] == "ATIVO":
                atual = operacoes[i]
                hora = operacoes[i]['hora']
                minutos = operacoes[i]['minutos']
                valorEntrada = operacoes[i]['valorEntrada']
                parDeMoeda = operacoes[i]['parDeMoeda']
                duracao = operacoes[i]['duracao']
                operacao = operacoes[i]['operacao']
                tipoConta = operacoes[i]['tipoConta']
                result = action(hora, minutos, valorEntrada, parDeMoeda, duracao, operacao, tipoConta)
                if(result == 'FIM'):
                    
                    oppp_ref.update({
                        'status': 'DESATIVADO'
                         })
                    break;
                break;       
            

                
                   


        while status == 'DESATIVADO':
            ref = db.reference('usuario/')
            snapshot = ref.get()
            robo = snapshot[idUser]['robo']
            status = robo['status']
            # print("desligado")
            if(status == 'PARAR'):
                break;